'use strict';

//Module declaration
var  platform = angular.module('platformApp', ['ngRoute']);

//Router configuration
platform.config(function($routeProvider, $httpProvider){

    $httpProvider.defaults.useXDomain = true;
    
    /*$routeProvider.when('/',{
	templateUrl : 'views/nomepagina.html',
	controller : 'pyramidsGiftCtrl'
    }).otherwise({redirectTo: '/'});*/
});

//Controller		
platform.controller('platformApp',function($scope, $http, $route, $location) {});
