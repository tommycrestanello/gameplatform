/**
* Matching Game : simple game in html and js
* by Crestanello Tommy
*/

var numberOfFaces = 5;
var theLeftSide = "";
var theRightSide = "";
var theBody = "";
var leftSideImages = "";

function generateFaces() {
    var iteration = numberOfFaces;
    theRightSide = document.getElementById("rightSide");
    theBody = document.getElementsByTagName("body")[0];
    theGame = document.getElementById("leftSide");
    
    while( iteration > 0 ) {
        theLeftSide = document.getElementById("leftSide");
	
        //Build the image to append 
        var images = document.createElement("img");

        images.setAttribute("src","smile.png");
        images.setAttribute("width",images.naturalWidth);
        images.setAttribute("height",images.naturalHeight);

        //Calculate the position in the div
        var randomTopPos = Math.floor(Math.random()*(theLeftSide.offsetWidth-images.naturalWidth));
        var randomLeftPos = Math.floor(Math.random()*(theLeftSide.offsetWidth-images.naturalHeight));

        //Setting the attribute style
        var styleParam = "left:"+parseInt(randomLeftPos)+"px; top:"+parseInt(randomTopPos)+"px";
        images.setAttribute("style",styleParam);
	
        //Append the image on the div
        document.getElementById("leftSide").appendChild(images);            
	
        iteration--;
    }

    //Cloning the images and complete the play ground
    leftSideImages = document.getElementById("leftSide").cloneNode(true);
    console.log(leftSideImages.id = "");
    var lastChildToRemove = leftSideImages.lastChild;
    leftSideImages.removeChild(lastChildToRemove);
    
    theRightSide.appendChild(leftSideImages);
    theLeftSide=document.getElementById("leftSide");
    
    //Event on lastChild 
    theLeftSide.lastChild.onclick = function nextLeve(event){
	console.log("sono qui ");
        event.stopPropagation();
        numberOfFaces+=5;
        cleanPlayArea();
        generateFaces();
        console.log(numberOfFaces);
    };

    //Event on the body
    theGame.onclick = function(event) {
        event.stopPropagation();
        alert("Game Over!! Try Again!!");
        theBody.onclick=null;
        theLeftSide.lastChild.onclick = null;

        //reload the level one in case of error
        window.location.reload();
    };
}

function cleanPlayArea() {
    while(theRightSide.firstChild) {
	theRightSide.removeChild(theRightSide.firstChild);
    }
    while(theLeftSide.firstChild) {
	theLeftSide.removeChild(theLeftSide.firstChild);
    }
}
